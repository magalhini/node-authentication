module.exports = function(app, passport) {
	// ================== HOMEPAGE
	app.get('/', function(req, res) {
		console.log(req.USER)
		res.render('index.ejs', {
			session: req.user
		});
	});

	// ================== LOGIN
	app.get('/login', function(req, res) {
		res.render('login.ejs', {
			message: req.flash('loginMessage')
		});
	});

	app.post('/login', passport.authenticate('local-login', {
		successRedirect: '/profile',
		failureRedirect: '/login',
		failureFlash: true
	}));
	
	// ================== SIGNUP
	app.get('/signup', function(req, res) {
		res.render('signup.ejs', {
			message: req.flash('signupMessage')
		});
	});

	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect: '/profile',
		failureRedirect: '/signup',
		failureFlash: true
	}));
	
	// ================== PROFILE
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user: req.user // get user out of session and pass onto template
		});
	});

	// ================== LOGOUT
	app.get('/logout', function(req, res) {
		req.logout();
		req.redirect('/');
	});

	// ================================
	// ================ FACEBOOK ROUTES
	// ================================
	
	// FB authentication and login
	app.get('/auth/facebook', passport.authenticate('facebook', {
		scope: 'email'
	}));

	app.get('/auth/facebook/callback', 
		passport.authenticate('facebook', {
			successRedirect: '/profile',
			failureRedirect: '/'
		})
	);
};

// router middleware to detect user session
function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}

	res.redirect('/');
}