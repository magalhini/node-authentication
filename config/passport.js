var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

var User = require('../app/models/user');
var configAuth = require('./auth');

module.exports = function(passport) {
	// ==================================
	// ============== LOCAL SIGNUP
	// ==================================
	// 
	// =============== session setup
	// required for persistent login sessions
	// passport needs the ability to serialize an unserialize users
	
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	// ==================================
	// ============== SIGNUP
	// ==================================

	passport.use('local-signup', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true // allows us to pass back the entire request to the callback
	},

	function(req, email, password, done) {
		process.nextTick(function () {

			// === checking to see if the user trying to login already exists
			User.findOne({ 'local.email': email }, function(err, user) {
				if (err) return done(err);

				if (user) {
					return done(null, false, req.flash('signupMessage', 'That email already exists'));
				} else {

					// no email found, create a new user
					var newUser = new User();

					newUser.local.email = email;
					newUser.local.password = newUser.generateHash(password);

					newUser.save(function(err) {
						if (err) throw err;
						return done(null, newUser);
					});
				}
			});
		});
	}
	));


	// ==================================
	// ============== LOGIN
	// ==================================
	
	passport.use('local-login', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},

	function(req, email, password, done) {
		User.findOne({ 'local.email': email }, function(err, user) {
			if (err) return done(err);

			// no user?
			if (!user) {
				return done(null, false, req.flash('loginMessage', 'No user was found'));
			}

			// wrong password?
			if (!user.validPassword(password)) {
				return done(null, false, req.flash('loginMessage', 'Ops, invalid password!'));
			}

			// got it!
			return done(null, user);
		});
	}));

	// ==================================
	// ============== FACEBOOK ==========
	// ==================================

	passport.use(new FacebookStrategy({

		clientID: configAuth.facebookAuth.clientID,
		clientSecret: configAuth.facebookAuth.clientSecret,
		callbackURL: configAuth.facebookAuth.callbackURL
	},

	function (token, refreshToken, profile, done) {
		console.log(profile)

		//async
		process.nextTick(function () {

			// find user in database based on facebook id
			User.findOne({'facebook.id': profile.id}, function (err, user) {
				if (err) return done(err);

				// log them in if the user exists
				if (user) {
					return done(null, user);
				} else {
					// otherwise, create them
					var newUser = new User();

					newUser.facebook.id = profile.id;
					newUser.facebook.token = token;
					newUser.facebook.name = profile.name.givenName;
					newUser.facebook.email = profile.emails[0].value;

					newUser.save(function (err) {
						if (err) throw err;

						// if success, return new user
						return done(null, newUser);
					});
				}
			});
		});
	}

	));
};